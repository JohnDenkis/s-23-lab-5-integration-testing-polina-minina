# Lab5 -- Integration testing


## InnoCar Specs

* Budet car price per minute = 16
* Luxury car price per minute = 40
* Fixed price per km = 15
* Allowed deviations in % = 11
* Inno discount in % = 10

## Creds

* KEY: AKfycbxReuav_j4FMBmT7i6ozd5E9oVjBOC7pKFyp5VlKjTRqmn7hjoeTDuzSayzMzTKKi2W
* EMAIL: p.minina@innopolis.university

## Testing

|  Type  |  Plan  |Distance|Planned Distance|  Time |Planned Time|Inno discount|Expected Result|Obtained Result| Conclusion      |
|--------|--------|--------|----------------|-------|------------|-------------|---------------|---------------|-----------------|
| luxury | minute | 62.3   | 70             | 106.8 | 120        |yes          |4699.2         |2990.4         |Computation Error|
| luxury | minute | 62.3   | 70             | 133.2 | 120        |yes          |5860.8         |3729.6         |Computation Error|
| luxury | minute | 62.23  | 70             | 106.68| 120        |yes          |4693.92        |2987.04        |Computation Error|
| luxury | minute | 77.77  | 70             | 133.32| 120        |yes          |5866.08        |3732.96        |Computation Error|
| luxury | minute | 70000  | 70             | 120000| 120        |yes          |5280000        |3360000        |Computation Error|
| luxury | minute | 0      | 70             | 0     | 120        |yes          |0              |0              |Success          |
| luxury | minute | 62.3   | 70             | 106.8 | 120        |no           |4272           |2990.4         |Computation Error|
| luxury | minute | 77.7   | 70             | 133.2 | 120        |no           |5328           |3729.6         |Computation Error|
| luxury | minute | 62.23  | 70             | 106.68| 120        |no           |4267.2         |2987.04        |Computation Error|
| luxury | minute | 77.77  | 70             | 133.32| 120        |no           |5332.79        |3732.96        |Computation Error|
| luxury | minute | 70000  | 70             | 120000| 120        |no           |4800000        |3360000        |Computation Error|
| luxury | minute | 0      | 70             | 0     | 120        |no           |0              |0              |Success          |
| budget | minute | 62.3   | 70             | 106.8 | 120        |yes          |1879.68        |2221.44        |Computation Error|
| budget | minute | 77.7   | 70             | 133.2 | 120        |yes          |2344.32        |2770.56        |Computation Error|
| budget | minute | 62.23  | 70             | 106.68| 120        |yes          |1877.568       |2218.944       |Computation Error|
| budget | minute | 77.77  | 70             | 133.32| 120        |yes          |2346.432       |2773.056       |Computation Error|
| budget | minute | 70000  | 70             | 120000| 120        |yes          |2112000        |2496000        |Computation Error|
| budget | minute | 0      | 70             | 0     | 120        |yes          |0              |0              |Success          |
| budget | minute | 62.3   | 70             | 106.8 | 120        |no           |1708.8         |2221.44        |Computation Error|
| budget | minute | 77.7   | 70             | 133.2 | 120        |no           |2131.2         |2770.56        |Computation Error|
| budget | minute | 62.23  | 70             | 106.68| 120        |no           |1706.88        |2218.944       |Computation Error|
| budget | minute | 77.77  | 70             | 133.32| 120        |no           |2133.12        |2773.056       |Computation Error|
| budget | minute | 70000  | 70             | 120000| 120        |no           |1920000        |2496000        |Computation Error|
| budget | minute | 0      | 70             | 0     | 120        |no           |0              |0              |Success          |
| budget | fixed_price | 70000 | 70         | 120000| 120        |yes          |2112000        |2000000        |Computation Error|
| budget | fixed_price | 0     | 70         | 0     | 120        |yes          |0              |0              |Success          |
| budget | fixed_price | 70000 | 70         | 120000| 120        |no           |1920000        |2000000        |Computation Error|
| budget | fixed_price | 0     | 70         | 0     | 120        |no           |0              |0              |Success          |
| budget | fixed_price | 70000 | 70         | 120000| 120        |yes          |2112000        |2000000        |Computation Error|
| budget | fixed_price | 0     | 70         | 0     | 120        |yes          |0              |0              |Success          |
| budget | fixed_price | 70000 | 70         | 120000| 120        |no           |1920000        |2000000        |Computation Error|
| budget | fixed_price | 0     | 70         | 0     | 120        |no           |0              |0              |Success          |

Total: 32
Passed: 8
Failed: 24

## Conclusion
* In case when distanse and time are greater than zero, there is a Computational Error.